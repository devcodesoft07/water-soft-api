<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run()
	{
		$this->call(RoleSeeder::class);
		// $this->call(MeterSeeder::class);
		$this->call(DistrictSeeder::class);
		$this->call(CommunitySeeder::class);
		$this->call(PaymentMethodSeeder::class);
		$this->call(PaymentOptionSeeder::class);
		$this->call(MeterCategorySeeder::class);
		$this->call(CoinSeeder::class);
		$this->call(ExpenseCategorySeeder::class);
	}
}

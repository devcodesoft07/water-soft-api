<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Coin;


class CoinSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $coin = Coin::create([
      'abbreviation' => 'Bs',
      'name' => 'Bolivianos'
    ]);

    $coin = Coin::create([
      'abbreviation' => '$us',
      'name' => 'Dolares Americanos'
    ]);
  }
}

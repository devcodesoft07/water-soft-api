<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PaymentMethod;

class PaymentMethodSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $payment_method = PaymentMethod::create([
      'name' => 'contado',
      'description' => 'Un solo pago'
    ]);

    $payment_method = PaymentMethod::create([
      'name' => 'cuotas',
      'description' => 'Pago en cuotas mensuales'
    ]);
  }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\MeterCategory;

class MeterCategorySeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $category = MeterCategory::create([
      'name' => 'casa',
      'description' => 'vivienda normal'
    ]);

    $category = MeterCategory::create([
      'name' => 'comercio',
      'description' => 'comercio publico'
    ]);
  }
}

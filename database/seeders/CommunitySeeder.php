<?php

namespace Database\Seeders;

use App\Models\Community;
use Illuminate\Database\Seeder;

class CommunitySeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Community::create([
      'district_id' => 1,
      'name' => 'Tajamar A'
    ]);

    Community::create([
      'district_id' => 1,
      'name' => 'Tajamar B'
    ]);

    Community::create([
      'district_id' => 1,
      'name' => 'Colque Rancho'
    ]);

    Community::create([
      'district_id' => 1,
      'name' => 'Laguna Centro'
    ]);

    Community::create([
      'district_id' => 1,
      'name' => 'Sulty Chico'
    ]);

    Community::create([
      'district_id' => 1,
      'name' => 'Sulty Grande'
    ]);
  }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\ExpenseCategory;


class ExpenseCategorySeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $category = ExpenseCategory::create([
      'name' => 'Luz',
      'description' => 'Energia Electrica'
    ]);

    $category = ExpenseCategory::create([
      'name' => 'Servicios',
      'description' => 'Servicios y Mantenimiento'
    ]);

    $category = ExpenseCategory::create([
      'name' => 'Defuncion',
      'description' => 'Defuncion de un socio'
    ]);

    $category = ExpenseCategory::create([
      'name' => 'Otros',
      'description' => 'Otros gastos'
    ]);
  }
}

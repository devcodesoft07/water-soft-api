<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PaymentOption;

class PaymentOptionSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $payment_option = PaymentOption::create([
      'name' => 'banco',
      'description' => 'Pago mediante banco'
    ]);

    $payment_option = PaymentOption::create([
      'name' => 'presencial',
      'description' => 'Pago presencial'
    ]);
  }
}

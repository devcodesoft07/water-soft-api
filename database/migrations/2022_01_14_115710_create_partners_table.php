<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartnersTable extends Migration
{
  /**
  * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('partners', function (Blueprint $table) {
      $table->id();
      $table->string('name');
      $table->string('last_name');
      $table->string('spouse')->nullable();
      $table->integer('community_id');
      $table->boolean('sewerage');
      $table->dateTimeTz('membership');
      $table->boolean('eliminated')->default(false);
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('partners');
  }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpensesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('expenses', function (Blueprint $table) {
      $table->id();
      $table->integer('expense_category_id');
      $table->integer('amount');//cantidad
      $table->integer('coin_id');//moneda
      $table->boolean('passage')->default(false);//pasaje
      $table->string('detail');//detalle
      $table->dateTimeTz('date');//fecha
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('expenses');
  }
}

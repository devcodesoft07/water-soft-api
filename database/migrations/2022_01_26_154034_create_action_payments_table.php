<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActionPaymentsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('action_payments', function (Blueprint $table) {
      $table->id();
      $table->integer('action_sale_id');
      $table->integer('payment_option_id');
      $table->decimal('amount');//costo
      $table->integer('quota');//couta
      $table->integer('penalty_fee')->default(0);//multa
      $table->string('voucher')->nullable();//comprobante
      $table->dateTimeTz('payment_date');//fecha de pago
      $table->dateTimeTz('next_payment');//proximo pago
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('action_payments');
  }
}

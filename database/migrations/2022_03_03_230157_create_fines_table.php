<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('fines', function (Blueprint $table) {
      $table->id();
      $table->integer('partner_id');
      $table->integer('amount');//monto
      $table->string('detail');//detalle
      $table->boolean('paid')->default(false);//pagado
      $table->dateTimeTz('date');//fecha de pago
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('fines');
  }
}

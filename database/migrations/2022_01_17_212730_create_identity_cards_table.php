<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIdentityCardsTable extends Migration
{
/**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('identity_cards', function (Blueprint $table) {
      // $table->id();
      $table->integer('ci_number')->id();
      $table->integer('partner_id');
      $table->string('issued');
      $table->string('complement')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('identity_cards');
  }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMeterSalesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('meter_sales', function (Blueprint $table) {
      $table->id();
      $table->integer('meter_id');
      $table->integer('payment_option_id');
      $table->integer('amount');
      $table->string('voucher')->nullable();
      $table->dateTimeTz('sale_date');//fecha de pago
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('meter_sales');
  }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActionSalesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('action_sales', function (Blueprint $table) {
      $table->id();
      $table->integer('action_id');
      $table->integer('payment_method_id');
      $table->integer('quotas_number');
      $table->integer('balance');//saldo
      $table->integer('debt');//deuda
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('action_sales');
  }
}

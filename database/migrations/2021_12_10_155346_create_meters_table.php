<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMetersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('meters', function (Blueprint $table) {
      $table->id();
      $table->string('meter_number')->unique();
      $table->decimal('unit_cost',16,2);
      $table->dateTimeTz('bought');
      $table->integer('partner_id')->nullable();
      $table->integer('category_id')->nullable();
      $table->boolean('active')->default(false);
      $table->string('observation');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('meters');
  }
}

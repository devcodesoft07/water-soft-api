<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class MeterFactory extends Factory
{
  /**
   * Define the model's default state.
   *
   * @return array
   */
  public function definition()
  {
    //regexify('^[a-zA-Z0-9-_.\s]+$')
    $active = $this->faker->randomElement($array = array (true,false));
    $observation = 'Ninguna';
    if (!$active) {
        $observation = $this->faker->randomElement($array = array ('En Corte','Disponible'));
    }
    return [
      'meter_number' => $this->faker->unique()->bothify('####??###'), // ' 42jz',
      'unit_cost'    => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = NULL),
      'bought'       => $this->faker->dateTime($max = 'now', $timezone = null),
      'observation'  => $observation,
      'active'       => $active,
    ];
  }
}

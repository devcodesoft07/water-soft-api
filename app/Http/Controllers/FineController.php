<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\FineRequest;

use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
//resources
use App\Http\Resources\FineResource;
use App\Http\Resources\FinesPartnerResource;

use App\Models\Fine;
use App\Models\Partner;
use App\Traits\ApiResponser;

class FineController extends Controller
{
  use ApiResponser;
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $paid = $request->get('paid');
    $month = $request->get('month');
    $year = $request->get('year');

    try {
      $fines = FineResource::collection(Fine::paid($paid)->month($month)->year($year)->paginate(10));

      return $fines;
    } catch (QueryException $e) {
      return $this->error(false, $e->getMessage(), 500);
    }
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\FineRequest  $request
   * @return \Illuminate\Http\Response
   */
  public function store(FineRequest $request)
  {
    try {
      $fine = new FineResource(Fine::create($request->all()));

      return $this->success(true, 'Multa registrada correctamente', $fine);
    } catch (QueryException $e) {
      return $this->error(false, $e->getMessage(), 500);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    try {
      $fine = new FineResource(Fine::FindOrFail($id));

      return $this->success(true, 'Multa obtenida correctamente', $fine);
    } catch (ModelNotFoundException $e) {
      return $this->error(false, 'multa con el id '.$id.' no existe', 400);
    } catch (QueryException $e) {
      return $this->error(false, $e->getMessage(), 500);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(FineRequest $request, $id)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }

  public function finesPartner()
  {
    try {
      $partners = FinesPartnerResource::collection(Partner::withFines()->paginate(10));

      return $partners;
    } catch (QueryException $e) {
      return $this->error(false, $e->getMessage(), 500);
    }
  }
}

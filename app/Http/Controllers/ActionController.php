<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ActionRequest;

//models
use App\Models\Action;
use App\Models\ActionPayment;

//tratis
use App\Traits\ApiResponser;
use App\Traits\ExceptionTrait;
use App\Traits\ActionSaleTrait;
use App\Traits\ActionPaymentTrait;
use App\Traits\ActionTrait;

use Throwable;

class ActionController extends Controller
{
  use ApiResponser;
  use Exceptiontrait;
  use ActionPaymentTrait;
  use ActionSaleTrait;
  use ActionTrait;

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {

  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\ActionRequest  $request
   * @return \Illuminate\Http\Response
   */
  public function store(ActionRequest $request)
  {
    $data_action = $request->safe()->only('partner_id', 'price');

    try {

      $action = $this->recordAction($data_action);

      $action_sale = $this->recordActionSale( $request->only(['payment_method', 'quotas_number']), $action );

      $this->recordPaymentAction($request->all(), $action_sale->id);

      $action->actionSale->actionPayments;

      return $this->success(true, 'action creada', $action);

    } catch (Throwable $th) {
      return $th;
      // return $this->exceptionCatched($th);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    try {
      $action = Action::findOrFail($id);

      $action->actionSale->actionPayments;

      return $this->success(true,'accion encontrada', $action);
    } catch (Throwable $th) {
      return $this->exceptionCatched($th, 'la accion con el id '.$id.' no existe');
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }
}

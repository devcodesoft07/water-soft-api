<?php

namespace App\Http\Controllers;
use App\Traits\ApiResponser;
use App\Http\Requests\MeterRequest;
use App\Http\Resources\MeterResource;
use App\Models\Meter;
use App\Services\MeterService;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;

class MeterController extends Controller
{
	use ApiResponser;
  protected $meterService;

  public function __construct(MeterService $meterService)
  {
    $this->meterService = $meterService;  
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    try {
      $month       = $request->get('month');
      $year        = $request->get('year');
      $state       = $request->get('active');
      $observation = $request->get('observation');

      $waterMeters = MeterResource::collection(Meter::orderMeter()->month($month)->year($year)->state($state)
                    ->observation($observation)->paginate(100))->response()->getData(true);

      $charts = $this->meterService->stadistics();   

      return $this->success(true, 'Medidores filtrados correctamente', ['meters' => $waterMeters, 'charts' => $charts ]);
    } catch (QueryException $e) {
      return $this->error(false, 'problemas internos en el servidor', 500);
    }
  }

  public function search(Request $request)
  {
    try {
      $meterNumber = $request->get('meterNumber');

      $waterMeters = Meter::orderMeter()->number($meterNumber)->paginate(100);

      return $this->success(true, 'Medidor buscado correctamente', $waterMeters);
    } catch (QueryException $e) {
      return $this->error(false, 'problemas internos en el servidor', 500);
    }
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\MeterRequest  $request
   * @return \Illuminate\Http\Response
   */
  public function store(MeterRequest $request)
  {
    try {
      //only record important data
      $new_meter = Meter::create($request->all());

      $meter = Meter::findOrFail($new_meter->id);

      return $this->success(true, 'medidor creado', $meter);

    } catch (ModelNotFoundException $e) {
      return $this->error(false, 'el medidor con el id no fue encontrado o no existe', 400);
    } catch (QueryException $e) {
      return $this->error(false, 'problemas internos en el servidor', 500);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    try {
      $meter = Meter::findOrFail($id);

      $meter->category;
      if ($meter->meter_sale) {
        $meter->meter_sale->payment_option;
      }
      $meter->partner;
      $meter->consumption;

      return $this->success(true, 'medidor encontrado', $meter);

    } catch (ModelNotFoundException $e) {
      return $this->error(false, 'el medidor con el id '.$id.' no fue encontrado o no existe', 400);
    } catch (QueryException $e) {
      return $this->error(false, 'problemas internos en el servidor', 500);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\MeterRequest  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(MeterRequest $request, $id)
  {
    try {
      $meter = Meter::findOrFail($id);

      $meter->update($request->all());

      return $this->success(true, 'medidor actualizado', $meter);

    } catch (ModelNotFoundException $e) {
      return $this->error(false, 'el medidor con el id '.$id.' no fue encontrado o no existe', 400);
    } catch (QueryException $e) {
      return $this->error(false, 'problemas internos en el servidor', 500);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    try {
      $meter = Meter::findOrFail($id);
      $meter->delete();

      return response()->json([
        'status' => true,
        'message' => 'Medidor eliminado exitosamente',
      ], 200);

    } catch (ModelNotFoundException $e) {
      return $this->error(false, 'el medidor con el id '.$id.' no fue encontrado o no existe', 400);
    } catch (QueryException $e) {
      return $this->error(false, 'problemas internos en el servidor', 500);
    }
  }
}

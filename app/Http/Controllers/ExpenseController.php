<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ExpenseRequest; 

//models
use App\Models\Expense;
//tratis
use App\Traits\ApiResponser;
use App\Traits\CoinTrait;

//reosurce
use App\Http\Resources\ExpenseResource;
use App\Services\ExpenseService;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class ExpenseController extends Controller
{
  use ApiResponser, CoinTrait;

  protected $expenseService;
  protected $expense;

  public function __construct(ExpenseService $expenseService)
  { 
    $this->expense = new Expense;
    $this->expenseService = $expenseService;
  }
  /**
   * Display a listing of the resource. 
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $category = $request->get('category');
    $month = $request->get('month');
    $year = $request->get('year');

    try {
      $expenses = ExpenseResource::collection($this->expense->category($category)->month($month)->year($year)->paginate(10))
                  ->response()->getData(true);
      $charts = $this->expenseService->stadistics();

      return $this->success(true, 'Gastos listados correctamente', ['expenses'=> $expenses, 'charts' => $charts]);
    } catch (QueryException $e) {
      return $this->error(false, $e->getMessage(), 500);
    }
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\RequeExpenseRequestst  $request
   * @return \Illuminate\Http\Response
   */
  public function store(ExpenseRequest $request)
  {
    try {
      $request->merge(['coin_id' => $this->getCoinId($request->coin)]);
      
      $expense = new ExpenseResource(Expense::create($request->all()));
      
      return $this->success(true, 'Gasto registrado correctamente', $expense, 201);
    } catch (QueryException $e) {
      return $this->error(false, $e->getMessage(), 500);
    }    
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    try {
      $expense = new ExpenseResource(Expense::findOrFail($id));
      
      return $this->success(true, 'Gasto con el id '.$id.' obtenido correctamente', $expense);
    } catch (ModelNotFoundException $e) {
      return $this->error(false, $e->getMessage(), 400);
    } catch (QueryException $e) {
      return $this->error(false, $e->getMessage(), 500);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RevenueRequest;

use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\Revenue;
use App\Services\RevenueService;
use App\Traits\ApiResponser;
use App\Traits\CoinTrait;
use Exception;

class RevenueController extends Controller
{

  use ApiResponser, CoinTrait;

  protected $revenueService;

  public function __construct(RevenueService $revenueService)
  {
    $this->revenueService = $revenueService;
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $month = $request->get('month');
    $year = $request->get('year');

    try {
      $revenues = Revenue::month($month)->year($year)->paginate(10);
      
      return $this->success(true, 'lista de ingresos varios', $revenues);
    } catch (QueryException $e) {
      return $this->error(false, $e->getMessage(), 500);
    }
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\RevenueRequest  $request
   * @return \Illuminate\Http\Response
   */
  public function store(RevenueRequest $request)
  {
    try {
      $request->merge(['coin_id' => $this->getCoinId($request->coin)]);
      
      $revenue = Revenue::create($request->all());

      return $this->success(true, 'ingreso registrado correctamente', $revenue);
    } catch (QueryException $e) {
      return $this->error(false, $e->getMessage(), 500);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    try {
      $revenue = Revenue::findOrFail($id);
      
      return $this->success(true, 'lista de ingresos varios', $revenue);
    } catch (ModelNotFoundException $e) {
      return $this->error(false, 'ingreso con el id '.$id. ' no existe', 400);
    } catch (QueryException $e) {
      return $this->error(false, $e->getMessage(), 500);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(RevenueRequest $request, $id)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }


 public function generalRevenues(Request $request)
 {
   $year = $request->get('year');
   $month = $request->get('month');
   $category = $request->get('category');

   try {
     $generalRevenues = $this->revenueService->indexGeneralRevenues($year, $month, $category);
     $charts = $this->revenueService->statdistics();

     return $this->success(true, 'lista general de ingresos',['generalRevenues' => $generalRevenues, 'charts' => $charts] );
   } catch (Exception $e) {
     return $e;
    // return $this->error(false, 'no se puedo obtener la lista de ingresos', 500);
   }
 }
}

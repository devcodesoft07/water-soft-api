<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\IdentityCard;

use App\Traits\ApiResponser;

use Illuminate\Database\QueryException;

class IdentityCardController extends Controller
{

  use ApiResponser;

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    //
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    try {
      if (IdentityCard::find($id)) {
        return $this->error(false, 'el carnet de identidad '. $id.' ya esta registrado', 400);
      }else {
        return $this->success(true, 'el carnet de identidad correcto es valido', []);
      }

    } catch (QueryException $th) {
      return $this->error(false, 'problemas en el servidor', 500);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }
}

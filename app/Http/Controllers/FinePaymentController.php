<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\FinePaymentRequest;

use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Exceptions\FinePaidException;

use App\Traits\ApiResponser;
use App\Traits\CoinTrait;
use App\Traits\FineTrait;

use App\Models\FinePayment;
use App\Http\Resources\FinePaymentResource;

class FinePaymentController extends Controller
{
  use ApiResponser;
  use CoinTrait;
  use FineTrait;

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    try {
      $fine_payments = FinePayment::paginate(10);

      return $this->success(true, 'pagos de multas', $fine_payments);
    } catch (QueryException $e) {
      return $this->error(false, $e->getMessage(), 500);
    }
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(FinePaymentRequest $request)
  {
    try {
      $this->paidFine($request->fine_id);
      $request->merge(['coin_id' => $this->getCoinId($request->coin)]);
      $fine_pay = new FinePaymentResource(FinePayment::create($request->all()));

      return $this->success(true, 'pago de la multa se realizo correctamente', $fine_pay);
    } catch (FinePaidException $e) {
      return $this->error(false, $e->getMessage(), 400);
    } catch (QueryException $e) {
      return $this->error(false, $e->getMessage(), 500);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    try {
      $fine_payment = new FinePaymentResource(FinePayment::findOrFail($id));

      return $this->success(true, 'pago de la multa con el id '.$id.' obtenida correctamente', $fine_payment);
    } catch (ModelNotFoundException $e) {
      return $this->error(false, 'pago de la multa con el id '.$id.' no existe', 400);
    } catch (QueryException $e) {
      return $this->error(false, $e->getMessage(), 500);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(FinePaymentRequest $request, $id)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }
}

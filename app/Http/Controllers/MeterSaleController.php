<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\MeterSaleRequest;
use App\Http\Resources\MeterSaleResource;
use App\Services\MeterSaleService;
use App\Services\MeterService;
use App\Traits\ApiResponser;
use Exception;

class MeterSaleController extends Controller
{
  use ApiResponser;

  protected $meterSaleService;

  public function __construct(MeterSaleService $meterSaleService)
  {
    $this->meterSaleService = $meterSaleService;
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(MeterSaleRequest $request, MeterService $meterService)
  {
    $meter_id = $request->meter_id;
    try {  
      $this->meterSaleService->meterHasPartnerId($meter_id);
      ($request->input('sold')) ? $this->meterSaleService->store($request->all()) : false;

      $meter = new MeterSaleResource($meterService->update($request->only('partner_id', 'category'), $meter_id));

      return $this->success(true, 'La asignacion de mededidor fue exitosa', $meter, 201);

    } catch (Exception $e) {
      return response()->json(['staus' => false,
      'message'=> 'no se pudo realizar la compra del medidor', 'error' => $e->getMessage()], $e->getCode());
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }
}

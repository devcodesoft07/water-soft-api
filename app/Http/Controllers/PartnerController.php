<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PartnerRequest;
use App\Http\Requests\PartnerUpdateRequest;
//resources
use App\Http\Resources\PartnerResource;

//traits
use App\Traits\ApiResponser;

//models
use App\Models\Partner;
use App\Services\CommunityService;
use App\Services\PartnerService;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PartnerController extends Controller
{
  use ApiResponser;
  protected $communityService;

  public function __construct(CommunityService $communityService)
  {
    $this->communityService = $communityService;
  }
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request, PartnerService $partnerService)
  {
    $eliminated = $request->get('eliminated');

    try {
      $partners = PartnerResource::collection(Partner::eliminated($eliminated)->paginate(50))
                ->response()->getData(true);
      $charts = $partnerService->stadisticPartner();

      return $this->success(true, 'lista actual de los socios', ['partners' => $partners, 'charts' => $charts]);
    } catch (QueryException $e) {
      return $this->error(false, $e->getMessage(), 500);
    }
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(PartnerRequest $request)
  {
    try {
      $request->merge(['community_id' => $this->communityService->getIdCommunity($request->community)]);

      $partner = new PartnerResource(Partner::create($request->except('ci_number')));

      return $this->success(true, 'socio registrado', $partner);

    } catch (Exception $e) {
      return $this->error(false, $e->getMessage(), 500);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    try {
      $partner = new PartnerResource(Partner::findOrFail($id));

      return $this->success(true, 'socio encontrado', $partner);

    } catch (ModelNotFoundException $e) {
      return $this->error(false, $e->getMessage(), 400);
    } catch (QueryException $e) {
      return $this->error(false, $e->getMessage(), 500);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\PartnerUpdateRequest  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(PartnerUpdateRequest $request, $id)
  {
    $request->merge(['community_id' => $this->communityService->getIdCommunity($request->community)]);

    try {
      $partner = Partner::findOrFail($id);
      $partner->update($request->all());

      return $this->success(true, 'socio actualizado correctamente', new PartnerResource($partner));
    } catch (ModelNotFoundException $e) {
      return $this->error(false,'el socio con el id '.$id.' no existe', 400);
    } catch (QueryException $e) {
      return $this->error(false, $e->getMessage(), 500);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    try {
      $partner = Partner::findOrFail($id);
      $partner->update(['eliminated' => true]);

      return $this->success(true, 'socio dado de baja', $partner);
    } catch(ModelNotFoundException $e){
      return $this->error(false, 'el socio con el id '.$id.' no existe', 400);
    }
  }
}

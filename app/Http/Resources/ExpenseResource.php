<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ExpenseResource extends JsonResource
{

  // public static $wrap = 'expense';
  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
   */
  public function toArray($request)
  {
    return [
      'id' => $this->id,
      'amount' => $this->amount,
      'coin'=> $this->coin->abbreviation,
      'passage' => $this->passage,
      'detail' => $this->detail,
      'date' => $this->date,
      'category' => [
        'id' => $this->expenseCategory->id,
        'name' => $this->expenseCategory->name,
        'description' => $this->expenseCategory->description
      ],
    ];
  }
}

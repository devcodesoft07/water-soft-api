<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FineForGeneralRevenuesResource extends JsonResource
{
  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
   */
  public function toArray($request)
  {
    return [
      'id' => $this->id,
      'category' => 'fines',
      'detail' => $this->detail,
      'partner' => $this->partner->name.' '. $this->partner->last_name,
      'payment' => $this->date,
      'amount' => $this->amount,
    ];
  }
}

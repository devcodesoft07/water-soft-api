<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class IdentityCardResource extends JsonResource
{
  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
   */
  public function toArray($request)
  {
    return [
      'ci_number' => $this->ci_number,
      'partner_id' => $this->partner_id,
      'issued' => $this->issued,
      'complement' => $this->complement,
      'created_at' => $this->created_at,
    ];
  }
}

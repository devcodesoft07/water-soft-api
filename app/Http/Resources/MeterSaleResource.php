<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MeterSaleResource extends JsonResource
{
  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
   */
  public function toArray($request)
  {
    //devolvemos el medidor y su venta
    return [
      'id' => $this->id,//medidor
      'meter_number' => $this->meter_number,//medidor
      'category_id' => $this->category_id, //categoria del medidor
      'active' => $this->active,//medidor
      'observation' => $this->observation,//medidor
      'meter_sale' => $this->meter_sale,//venta del medidor
    ];
  }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ActionSaleResource extends JsonResource
{
  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
   */
  public function toArray($request)
  {
    return [
      'id' => $this->id,
      'payment_method_id' => $this->payment_method_id,
      'quotas_number' => $this->quotas_number,
      'balance' => $this->balance,
      'debt' => $this->debt,
      'created_at' => $this->created_at,
      'action_payments' => $this->actionPayments
    ];
  }
}

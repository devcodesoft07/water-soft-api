<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FineResource extends JsonResource
{
  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
   */
  public function toArray($request)
  {
    return [
      'id' => $this->id,
      'amount' => $this->amount,
      'detail' => $this->detail,
      'partner' => $this->partner->last_name.' '. $this->partner->name,
      'paid' => $this->paid,
      'date' => $this->date
    ];
  }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MeterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
      return [
        'id' => $this->id,
        'meter_number' => $this->meter_number,
        'unit_cost' => $this->unit_cost,
        'bought' => $this->bought,
        'category_id' => $this->category_id,
        'active' => $this->active,
        'observation' => $this->observation,
        'created_at' => $this->created_at,
      ];
    }
}

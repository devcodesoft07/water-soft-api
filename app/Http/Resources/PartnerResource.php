<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

//resources
use App\Http\Resources\MeterResource;
use App\Http\Resources\ActionResource;
use App\Http\Resources\CommunityResource;
use App\Http\Resources\IdentityCardResource;
use App\Http\Resources\TelephoneResource;

//traits
use App\Traits\ActionPaymentTrait;


class PartnerResource extends JsonResource
{
  use ActionPaymentTrait;

  // public static $wrap = 'partner';

  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
   */
  public function toArray($request)
  {
    return [
      'id' => $this->id,
      'name' => $this->name,
      'last_name' => $this->last_name,
      'spouse' => $this->spouse,
      'sewerage' => $this->sewerage,
      'eliminated' => $this->eliminated,
      'membership' => $this->membership,
      'created_at' => $this->created_at,
      'identity_card' => new IdentityCardResource($this->identity_card),
      'debtor' => $this->when($this->action, function () {
        return $this->isDebtor($this->id);//cuando sea la ultima cuota, implementar
      }),
      'meters' => MeterResource::collection($this->meters),
      'community' => new CommunityResource($this->community),
      'telephones' => TelephoneResource::collection($this->telephones),
      'action' => $this->when($this->action, function () {
        return new ActionResource($this->action);
      }),
    ];
  }
}

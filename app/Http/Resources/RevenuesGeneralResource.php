<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RevenuesGeneralResource extends JsonResource
{
  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
   */
  public function toArray($request)
  {
    return [
      'id' => $this->id,
      'category' => 'otros',
      'detail' => $this->detail,
      'partner' => '',
      'date' => $this->date,
      'amount' => $this->amount,
      'coin' => $this->coin->abbreviation
    ];
  }
}

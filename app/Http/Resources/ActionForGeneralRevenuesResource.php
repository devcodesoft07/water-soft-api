<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ActionForGeneralRevenuesResource extends JsonResource
{
  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
   */
  public function toArray($request)
  {
    $partner = $this->actionSale->action->partner;
    return [
      'id' => $this->id,
      'category' => 'action',
      'detail' => $this->actionSale->action->id,
      'partner' => $partner->name.' '. $partner->last_name,
      'payment_date' => $this->payment_date,
      'amount' => $this->amount
    ];
  }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MeterGeneralRevenuesResource extends JsonResource
{
  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
   */
  public function toArray($request)
  {
    $partner = $this->meter->partner;
    return [
      'id' => $this->meter_id,
      'category' => 'meters',
      'detail' => 'Compra de medidor',
      'partner' => $partner->name.' '. $partner->last_name,
      'sale_date' => $this->sale_date,
      'amount' => $this->amount,
      'coin' => 'Bs'
    ];
  }
}

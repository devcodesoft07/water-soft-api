<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

//resources
use App\Http\Resources\ActionSaleResource;

class ActionResource extends JsonResource
{
  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
   */
  public function toArray($request)
  {
    return [
      'id' => $this->id,
      'action_code' => $this->action_code,
      'price' => $this->price,
      'created_at' => $this->created_at,
      'action_sale' => new ActionSaleResource($this->actionSale)
    ];
  }
}

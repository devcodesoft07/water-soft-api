<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Models\Meter;

class MeterRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'meter_number' => [
        'required',
        'string',
        'regex:/^[a-zA-Z0-9-_.\s]+$/',
        'min:1', 'max:9',
        Rule::unique('meters', 'meter_number')->ignore($this->meter)
      ],
      'unit_cost' => [
        'required',
        'numeric',
        'min:1',
        'regex:/^[\d]{0,14}(\.[\d]{1,2})?$/'
      ],
      'bought' => 'required|date|before:tomorrow',
      'partner_id' => 'nullable|numeric',
      'category_id' => 'exclude_if:partner_id,null|nullable|numeric',
      'active' => 'nullable|boolean',
      'observation' => [
        'required_with:active',
        'string',
        'regex:/^[A-Za-z0-9_! "#$%&\'()*+,.:\/;=?@^-]+$/']
      ];
  }
}

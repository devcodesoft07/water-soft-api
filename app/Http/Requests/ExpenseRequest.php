<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use App\Models\Coin;
use App\Models\ExpenseCategory;


class ExpenseRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'amount' => 'required|numeric|min:1',
      'coin' => 'required|string|exists:coins,abbreviation',
      'expense_category_id' => 'required|numeric|exists:expense_categories,id',
      'passage' => 'nullable|boolean',
      'detail' => 'required|string',
      'date' => 'required|date|before:tomorrow'
    ];
  }
}

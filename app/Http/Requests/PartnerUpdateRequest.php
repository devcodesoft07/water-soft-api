<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;


class PartnerUpdateRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    $phone = 'string|min:7|max:15|regex:/^[23467][0-9]+$/';
  
  return [
    'name' => [
      'string',
      'min:2',
      'max:255',
      'regex:/^[a-zA-Z áéíóúÁÉÍÓÚñÑ]+$/'
    ],
    'last_name'=> [
      'string',
      'min:2',
      'max:255',
      'regex:/^[a-zA-Z áéíóúÁÉÍÓÚñÑ]+$/'
    ],
    'ci_number'=> [
      'numeric',
      'digits_between:6,13',
      Rule::unique('identity_cards', 'ci_number')->ignore($this->ci_number, 'ci_number')
    ],
    'complement'=> 'nullable|alpha_num|size:2',
    'issued'=> 'string|alpha|size:2',//expedido
    'first_phone'=> $phone,
    'spouse'=> [
      'nullable',
      'string',
      'min:2',
      'max:255',
      'regex:/^[a-zA-Z áéíóúÁÉÍÓÚñÑ]/'
    ],//conyuge
    'community'=> 'required|string|min:2|max:255|exists:communities,name',
    'membership'=> 'date|before:tomorrow',//fecha de afilicion
    'sewerage'=> 'boolean',//alcantarillado
    'second_phone'=> 'nullable|'.$phone, 
    'third_phone'=> 'nullable|'.$phone
  ];

  }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Meter;
use App\Models\Partner;
use App\Models\MeterCategory;
use App\Models\PaymentOption;

class MeterSaleRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'meter_id' => 'required|numeric|exists:meters,id',
      'partner_id' => 'required|numeric|exists:partners,id',
      'sold' => 'required|boolean',
      'category' => 'required|string|exists:meter_categories,name',
      'payment_way' => 'required|string|exists:payment_options,name',
      'voucher' => 'required_if:payment_way,banco|string|url',
      'amount' => 'required|numeric|min:1',
      'sale_date' => 'required|date|before:tomorrow'
    ];
  }
}

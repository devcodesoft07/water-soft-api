<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use App\Models\Fine;
use App\Models\Coin;

class FinePaymentRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'fine_id' => 'required|numeric|numeric|exists:fines,id',
      'coin' => 'required|string|exists:coins,abbreviation',
      'date'=> 'required|date|before:tomorrow'
    ];
}
}

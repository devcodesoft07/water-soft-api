<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use App\Models\Coin;

class RevenueRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'amount' => 'required|numeric|min:1',
      'coin' => 'required|string|exists:coins,abbreviation',
      'detail' => 'required|string|min:5',
      'date' => 'required|date|before:tomorrow'
    ];
  }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\PaymentMethod;
use App\Models\PaymentOption;
use App\Models\Partner;
use App\Models\Action;


class ActionRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'partner_id' => 'required|numeric|exists:partners,id|unique:actions,partner_id',
      'price' => 'required|numeric',//precio de la accion

      'payment_method' => 'required|string|exists:payment_methods,name',
      'quotas_number' => 'required|numeric|max:4',

      'payment_way' => 'required|string|exists:payment_options,name',//o por banco
      'amount' => 'required|numeric|min:1|regex:/^[\d]{0,14}(\.[\d]{1,2})?$/',//monto del primer pago
      'quota' => 'required|numeric|min:1|max:4',//couta 1 0 2 0 3 0 4
      'penalty_fee' => 'nullable|numeric',
      'voucher' => 'required_if:payment_way,banco|url',//o por banco
      'payment_date' => 'required|date|before:tomorrow',
      'next_payment' => 'required|date|after:yesterday'
    ];
  }
}

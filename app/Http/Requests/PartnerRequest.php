<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PartnerRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    $phone = 'string|min:7|max:15|regex:/^[23467][0-9]+$/';
    
    return [
      'name' => [
        'required',
        'string',
        'min:2',
        'max:255',
        'regex:/^[a-zA-Z áéíóúÁÉÍÓÚñÑ]+$/'
      ],
      'last_name'=> [
        'required',
        'string',
        'min:2',
        'max:255',
        'regex:/^[a-zA-Z áéíóúÁÉÍÓÚñÑ]+$/'
      ],
      'ci_number'=> [
        'required',
        'numeric',
        'digits_between:6,13',
        Rule::unique('identity_cards', 'ci_number')->ignore($this->identity_card)
      ],
      'complement'=> 'nullable|alpha_num|size:2',
      'issued'=> 'required|string|alpha|size:2',//expedido
      'first_phone'=> 'required|'. $phone,
      'spouse'=> [
        'nullable',
        'string',
        'min:2',
        'max:255',
        'regex:/^[a-zA-Z áéíóúÁÉÍÓÚñÑ]/'
      ],//conyuge
      'community'=> 'required|string|min:2|max:255|exists:communities,name',
      'membership'=> 'required|date|before:tomorrow',//fecha de afilicion
      'sewerage'=> 'required|boolean',//alcantarillado
      'second_phone'=> 'nullable|'.$phone, 
      'third_phone'=> 'nullable|'.$phone
    ];
  }
}

<?php

namespace App\Services;

use Illuminate\Container\Container;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class PaginationService {

  public static function paginate(Collection $collection, $pageSize)
  {
    $page = Paginator::resolveCurrentPage('general-revenues');

    $total = $collection->count();


    return self::paginator($collection->forPage($page, $pageSize), $total, $pageSize, $page, [
      'path' => Paginator::resolveCurrentPath(),
      'pageName' => 'page'
    ]);
  }

  public static function paginator($items, $total, $perPage, $currentPage, $options)
  {
    return Container::getInstance()->makeWith(LengthAwarePaginator::class, compact(
      'items', 'total', 'perPage', 'currentPage', 'options'
    ));
  }
}
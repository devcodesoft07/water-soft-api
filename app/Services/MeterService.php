<?php

namespace App\Services;

use App\Models\Meter;
use App\Models\MeterCategory;
use Exception;

class MeterService {

  public function stadistics()
  {
    $charts = [];    
    $meters = Meter::all();

    if ($meters->count() != 0) {
      $charts = [
        [
          "name" => 'Activos',
          'value' => $meters->where('active', 1)->count()
        ],
        [
          "name" => 'En Corte',
          'value' => $meters->where('observation', 'En Corte')->count()
        ],
        [
          "name" => 'Disponible',
          'value' => $meters->where('observation', 'Disponible')->count()
        ],
        [
          "name" => 'Otros Inactivos',
          'value' => $meters->whereNotIn('observation', ['Disponible', 'En Corte'])
                                ->where('active', 0)->count()
        ]
      ];
    }
    return $charts;
  }


  public function update(array $data, $id):Meter
  {
    try {
      ($data['category'])
        ? $data['category_id'] = MeterCategory::where('name', $data['category'])->first()->id
        : null;

      if ($data['partner_id']) {
        $data['active'] = true;
        $data['observation'] = 'Ninguna';
      }

      $meter = tap(Meter::findOrFail($id))->update($data);
      return $meter;
    } catch (Exception $e) {
      throw new Exception('no se pue actualizar el medidor', 500);
    }
  }

  public function descative($data, $meter_id):void
  {
    Meter::findOrFail($meter_id)->update($data);
  }
}
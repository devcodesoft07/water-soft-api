<?php

namespace App\Services;

use App\Models\IdentityCard;

class IdentityCardService {

  public function register(array $data, int $partner_id)
  {
    try {

      IdentityCard::create($data + ['partner_id' => $partner_id]);

    } catch (\Exception $e) {
      return response()->json([
        'status' => false,
        'message' => 'no se pudo registar el carnet',
        'error' => $e->getMessage()], 500);
    }
  }

  public function update($data, $ci_number)
  {
    try {
      IdentityCard::findOrFail($ci_number)->update($data);
    } catch (\Exception $e) {
      return response()->json([
        'status' => false,
        'message' => 'no se pudo registar el carnet',
        'error' => $e->getMessage()], 500);
    }
  }
}
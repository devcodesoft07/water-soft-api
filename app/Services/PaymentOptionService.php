<?php

namespace App\Services;

use App\Models\PaymentOption;

class PaymentOptionService {

  public function getId($payment) :int
  {
    return PaymentOption::where('name', $payment)->first()->id;
  }
}
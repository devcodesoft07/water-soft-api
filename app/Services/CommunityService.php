<?php
namespace App\Services;

use App\Models\Community;
use App\Models\District;

class CommunityService {

  public function storeCommunity($name)
  {
    try {
      $district = District::firstOrCreate( ['name' => 'Distrito 4'] );

      $community = Community::firstOrCreate(
        ['name' => $name],
        ['district_id' => $district->id]
      );

      return $community->id;

    } catch (\Exception $e) {
      return response()->json([
        'status' => false,
        'message' => 'no se pudo registrar la comunidad',
        'error' => $e->getMessage()
      ], 500);
    }
  }

  public function getIdCommunity($community):int
  {
   $community = Community::where('name', $community)->first();

   return $community->id;
  }
}
<?php

namespace App\Services;

use App\Models\Meter;
use App\Models\MeterSale;
use Exception;
use Facade\FlareClient\Http\Response;

class MeterSaleService {

  protected $paymentOptionService;

  function __construct(PaymentOptionService $paymentOptionService)
  {
   $this->paymentOptionService = $paymentOptionService;
  }

  public function store(array $data)
  {
    try {
      $data['payment_option_id'] = $this->paymentOptionService->getId($data['payment_way']); 

      MeterSale::create($data);

    } catch (Exception $e) {
      throw new Exception('No se puedo registrar la venta del medidor', 500);
    }
  }

  public function meterHasPartnerId($meter_id)
  {
    $meter = Meter::find($meter_id);

    if ($meter->partner_id) {
      throw new Exception('el medidor ya tiene duenio',400);
    }
  }
}
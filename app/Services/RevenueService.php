<?php

namespace App\Services;

use App\Http\Resources\ActionForGeneralRevenuesResource;
use App\Http\Resources\FineForGeneralRevenuesResource;
use App\Http\Resources\MeterGeneralRevenuesResource;
use App\Http\Resources\RevenuesGeneralResource;
use App\Models\ActionPayment;
use App\Models\Fine;
use App\Models\MeterSale;
use App\Models\Revenue;
use Carbon\CarbonPeriod;

class RevenueService {

  protected $actionPayment;
  protected $meterSale;
  protected $finePayement;
  protected $revenue;
  protected $paginationService;

  public function __construct(PaginationService $paginationService)
  {
    $this->actionPayment = new ActionPayment;
    $this->meterSale = new MeterSale;
    $this->fine = new Fine(['paid' => true]);
    $this->revenue = new Revenue;
    $this->paginationService = $paginationService;
  }

  public function indexGeneralRevenues($year, $month, $category)
  {
    $generalRevenues = collect();
    $actions = ActionForGeneralRevenuesResource::collection($this->actionPayment->year($year)->month($month)->get());
    $meterSales = MeterGeneralRevenuesResource::collection($this->meterSale->year($year)->month($month)->get());
    $fines = FineForGeneralRevenuesResource::collection($this->fine->year($year)->month($month)->get());
    $revenues = RevenuesGeneralResource::collection($this->revenue->year($year)->month($month)->get());

    if($category){
      switch ($category) {
        case 'actions':
          $generalRevenues = $generalRevenues->merge($actions);
          break;
  
        case 'meters':
          $generalRevenues = $generalRevenues->merge($meterSales);
          break;
  
        case 'fines':
          $generalRevenues = $generalRevenues->merge($fines);
          break;

        case 'otros':
          $generalRevenues = $generalRevenues->merge($revenues);
          break;
      }
      return $this->paginationService->paginate($generalRevenues, 10);

    }else {
      $generalRevenues = $generalRevenues->merge([$actions, $meterSales,$fines,$revenues])->flatMap(function($item){
        return $item->values();
      });
      return $this->paginationService->paginate(collect($generalRevenues), 10);
    }
  }

  public function statdistics()
  {
    $year = now()->year;
    $categories = collect(['actions' => $this->actionPayment,
      'meterSales' => $this->meterSale,
      'fines' => $this->fine,
      'otros' => $this->revenue,
    ]);

    $categoryRevenues = $categories->map(function($category, $key) use($year){
      return [
        'name' => $key,
        'value' => $category->year($year)->sum('amount')
      ];
    });

    $annualRevenues = collect(CarbonPeriod::create($year.'-01-01', '1 month', $year.'-12-01'))
      ->map(function($month) use($categories, $year){
        return [
          'name' => $month->format('M'),
          'value' => $categories->map(function($category) use($year, $month) {
            return $category->year($year)->month($month->format('m'))->sum('amount');
          })->sum()
        ];
    });

     return [
      'categoryRevenues' => $categoryRevenues->values(),
      'annualRevenues'  => $annualRevenues,
      'totalrevenues' => ['year'=> $year, 'total' => $categories->map(function($category) use($year) { 
        return $category->year($year)->sum('amount');
      })->sum()]
    ];
  }
}
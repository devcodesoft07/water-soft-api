<?php
namespace App\Services;

use App\Models\Expense;
use App\Models\ExpenseCategory;
use Carbon\CarbonPeriod;

class ExpenseService {

  public function stadistics()
  {
    $year = now()->year;
    $expenses = new Expense;

    $categoryExpenses = ExpenseCategory::all()->map(function($category) use($expenses, $year){
      return [
        'name' => $category->name,
        'value' => $expenses->year($year)->category($category->id)->sum('amount')
      ];
    });

    $annualExpenses = collect(CarbonPeriod::create($year.'-01-01', '1 month', $year.'-12-01'))
      ->map(function($month) use($expenses, $year){
        return [
          'name' => $month->format('M'),
          'value' => $expenses->year($year)->month($month->format('m'))->sum('amount')
        ];
      });

    $charts = [
      'categoryExpenses' => $categoryExpenses,
      'annualExpenses'  => $annualExpenses,
      'totalExpenses' => ['year'=> $year, 'total' => $expenses->year($year)->sum('amount')]
    ];

    return $charts;
  }
}
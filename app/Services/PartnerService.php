<?php

namespace App\Services;

use App\Models\Meter;

class PartnerService {

  protected $meterService;
  protected $meters;

  function __construct(MeterService $meterService)
  {
    $this->meters = new Meter;
    $this->meterService = $meterService;
  }

  public function stadisticPartner()
  {
    $meters = $this->meters->whereNotNull('partner_id')->get();
    
    $charts = [
      [
        'name' => 'Activos',
        'value' => $meters->where('active', true)->count()
      ],
      [
        'name' => 'En Corte',
        'value' => $meters->where('observation', 'En Corte')->count()
      ],
      [
        'name' => 'Deudores',
        'value' => 10
      ],
    ];

    return $charts;
  }

  public function desactivePartnerMeter($meters)
  {
    $data_meter = ['partner_id' => null, 'active' => false, 'observation' => 'Disponible'];
    foreach ($meters as $meter) {
      $this->meterService->descative( $data_meter, $meter->id );
    }
  }

}
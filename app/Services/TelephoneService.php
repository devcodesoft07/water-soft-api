<?php

namespace App\Services;

use App\Models\Partner;
use App\Models\Telephone;
use Exception;

class TelephoneService {

  public function storeTelephones(array $phones, Partner $partner)
  {
    array_filter($phones);
    try {
      foreach ($phones as $phone) {
        $telephone = Telephone::firstOrCreate( ['number' => $phone]);
        $partner->telephones()->attach($telephone->id);
      }

    } catch (Exception $e) {
      return response()->json([
        'status' => false,
        'msg' => 'no se pudo registrar los numeros',
        'errror' => $e->getMessage()], 500);
    }
  }

  public function updateTelephone($phones, Partner $partner)
  {
    $new_phones = [];
    array_filter($phones);
    try {
      foreach ($phones as $phone) {
        $phn = Telephone::firstOrCreate(['number' => $phone]);
        $new_phones[] = $phn->id;
      }

      $partner->telephones()->sync($new_phones);

    } catch (Exception $e) {
      return response()->json([
        'status' => false,
        'msg' => 'no se pudo registrar los numeros',
        'errror' => $e->getMessage()], 500);
    }
  }
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Meter;

class MeterCategory extends Model
{
  use HasFactory;

  protected $fillable = [
    'name',
    'description'
  ];

  public function meters()
  {
    return $this->hasMany(Meter::class);
  }
}

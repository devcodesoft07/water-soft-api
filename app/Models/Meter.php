<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\MeterCategory;
use App\Models\MeterSale;
use App\Models\Partner;

class Meter extends Model
{
  use HasFactory;

  protected $fillable = [
    'meter_number',
    'unit_cost',
    'bought',
    'partner_id',
    'category_id',
    'active',
    'observation'
  ];


  //relationships function
  public function category() {
    return $this->belongsTo(MeterCategory::class);
  }

  public function meter_sale() {
    return $this->hasOne(MeterSale::class);
  }

  public function partner() {
    return $this->belongsTo(Partner::class);
  }

  //scope functions
  public function scopeNumber($query, $meterNumber) {
    if ($meterNumber) {
      return $query->where('meter_number', $meterNumber);
    }
  }

  public function scopeObservation($query, $observation) {
    if ($observation) {
      return $query->where('observation', $observation);
    }
  }

  public function scopeState($query, $state) {
    if (isset($state)) {
      return $query->where('active', $state);
    }    
  }

  public function scopeYear($query, $year) {
    if ($year) {
      return $query->whereYear('bought', $year);
    }
  }

  public function scopeMonth($query, $month) {
    if ($month) {
      return $query->whereMonth('bought', $month);
    }
  }

  public function scopeOrderMeter($query) {
      return $query->orderBy('active', 'desc')
                    ->orderBy('bought', 'DESC');
  }
}

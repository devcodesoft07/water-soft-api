<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\Relations\Pivot;

class PartnerPhone extends Model
{
  use HasFactory;

  protected $fillable = [
    'partner_id',
    'telephone_id'
  ];
}

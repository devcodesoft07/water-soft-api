<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\IdentityCard;
use App\Models\Community;
use App\Models\Telephone;
use App\Models\Meter;
use App\Models\Action;
use App\Models\Fine;


class Partner extends Model
{
  use HasFactory;

  protected $fillable = [
    'name',
    'last_name',
    // 'ci_number',
    'spouse',
    'community_id',
    'sewerage',
    'membership',
    'eliminated'
  ];

  //relationship functions
  public function identity_card()
  {
    return $this->hasOne(IdentityCard::class);
  }

  public function community()
  {
    return $this->belongsTo(Community::class);
  }

  public function telephones()
  {
    return $this->belongsToMany(Telephone::class, 'partner_phones', 'partner_id', 'telephone_id');
  }

  public function meters()
  {
    return $this->hasMany(Meter::class);
  }

  public function fines()
  {
    return $this->hasMany(Fine::class);
  }

  public function action()
  {
    return $this->hasOne(Action::class);
  }

  //scope functions
  public function scopeEliminated($query, $eliminated)
  {
    if ($eliminated) {
      return $query->where('eliminated', $eliminated);
    }else {
      return $query->where('eliminated', false);
    }
  }

  public function scopeWithFines($query)
  {
    return $query->with('fines')->whereRelation('fines', 'paid', false);
  }
}

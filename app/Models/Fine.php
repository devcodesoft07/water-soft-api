<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Partner;
use App\Models\FinePayment;

class Fine extends Model
{
  use HasFactory;

  protected $fillable = [
    'partner_id',
    'amount',
    'detail',
    'paid',
    'date'
  ];

  public function partner()
  {
    return $this->belongsTo(Partner::class);
  }

  public function finePayment()
  {
    return $this->hasOne(FinePayment::class);
  }


  //scope functions
  public function scopeMonth($query, $month)
  {
    if ($month) {
      return $query->whereMonth('date', $month);
    }
  }

  public function scopeYear($query, $year)
  {
    if ($year) {
      return $query->whereYear('date', $year);
    }
  }

  public function scopePaid($query, $paid)
  {
    if ($paid) {
      return $query->where('paid', $paid);
    }
  }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Modles\ActionSale;
use App\Modles\PaymentOption;

class ActionPayment extends Model
{
  use HasFactory;

  protected $fillable = [
    'action_sale_id',
    'payment_option_id',
    'amount',
    'quota',
    'penalty_fee',
    'voucher',
    'payment_date',
    'next_payment'
  ];

  public function actionSale()
  {
    return $this->belongsTo(ActionSale::class);
  }

  public function paymentOption()
  {
    return $this->belongsTo(PaymentOption::class);
  }

  //scope functions
  public function scopeMonth($query, $month)
  {
    if ($month) {
      return $query->whereMonth('payment_date', $month);
    }
  }

  public function scopeYear($query, $year)
  {
    if ($year) {
      return $query->whereYear('payment_date', $year);
    }
  }
}

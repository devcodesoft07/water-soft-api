<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Partner;

class Telephone extends Model
{
  use HasFactory;
  
  protected $fillable = [
    'number'
  ];

  public function partners()
  {
    return $this->belongsToMany(Partner::class, 'partner_phones', 'telephone_id', 'partner_id');
  }

}

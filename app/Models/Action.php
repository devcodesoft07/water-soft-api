<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Partner;
use App\Models\ActionSale;

class Action extends Model
{
  use HasFactory;

  protected $fillable = [
    'action_code',
    'partner_id',
    'price'
  ];

  public function partner()
  {
    return $this->belongsTo(Partner::class);
  }

  public function actionSale()
  {
    return $this->hasOne(ActionSale::class);
  }
}

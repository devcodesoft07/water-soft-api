<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Meter;
use App\Models\PaymentOption;

class MeterSale extends Model
{
  use HasFactory;

  protected $fillable = [
    'meter_id',
    'payment_option_id',
    'amount',
    'voucher',
    'sale_date'
  ];

  //relationship function
  public function meter() {
    return $this->belongsTo(Meter::class);
  }

  public function payment_option() {
    return $this->belongsTo(PaymentOption::class);
  }

  //scope functions
  public function scopeMonth($query, $month)
  {
    if ($month) {
      return $query->whereMonth('sale_date', $month);
    }
  }

  public function scopeYear($query, $year)
  {
    if ($year) {
      return $query->whereYear('sale_date', $year);
    }
  }
}

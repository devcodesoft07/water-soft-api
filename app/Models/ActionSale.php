<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Action;
use App\Models\PaymentMethod;
use App\Models\ActionPayment;


class ActionSale extends Model
{
  use HasFactory;

  protected $fillable = [
    'action_id',
    'payment_method_id',
    'quotas_number',
    'balance',
    'debt'
  ];

  public function action()
  {
    return $this->belongsTo(Action::class);
  }

  public function paymentMethod()
  {
    return $this->belongsTo(PaymentMethod::class);
  }

  public function actionPayments()
  {
    return $this->hasMany(ActionPayment::class);
  }
}

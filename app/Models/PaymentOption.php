<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\ActionPayment;


class PaymentOption extends Model
{
  use HasFactory;

  protected $fillable = [
    'name',
    'description'
  ];

  public function actionPayments()
  {
    return $this->hasMany(ActionPayment::class);
  }
}

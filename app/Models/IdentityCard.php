<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Partner;


class IdentityCard extends Model
{
  use HasFactory;
  
  protected $primaryKey = 'ci_number';

  protected $fillable = [
    'ci_number',
    'partner_id',
    'issued',
    'complement'
  ];

  public function partner()
  {
    return $this->belongsTo(Partner::class);
  }

}

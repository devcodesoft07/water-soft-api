<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Coin;
use App\Models\ExpenseCategory;


class Expense extends Model
{
  use HasFactory;

  protected $fillable = [
    'expense_category_id',
    'amount',
    'coin_id',
    'passage',
    'detail',
    'date'
  ];

  public function coin()
  {
    return $this->belongsTo(Coin::class);
  }

  public function expenseCategory()
  {
    return $this->belongsTo(ExpenseCategory::class);
  }

  //scope functions
  public function scopeMonth($query, $month)
  {
    if ($month) {
      return $query->whereMonth('date', $month);
    }
  }

  public function scopeYear($query, $year)
  {
    if ($year) {
      return $query->whereYear('date', $year);
    }
  }

  public function scopeCategory($query, $category)
  {
    if ($category) {
      return $query->where('expense_category_id', $category);
    }
  }
}

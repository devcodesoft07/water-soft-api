<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Coin;

class Revenue extends Model
{
  use HasFactory;

  protected $fillable = [
    'amount',
    'coin_id',
    'detail',
    'date'
  ];

  public function coin()
  {
    return $this->belongsTo(Coin::class);
  }

  //scope functions
  public function scopeMonth($query, $month)
  {
    if ($month) {
      return $query->whereMonth('date', $month);
    }
  }

  public function scopeYear($query, $year)
  {
    if ($year) {
      return $query->whereYear('date', $year);
    }
  }
}

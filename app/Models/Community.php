<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Partner;
use App\Models\District;

class Community extends Model
{
  use HasFactory;

  public function partners()
  {
    return $this->hasMany(Partner::class);
  }

  public function district()
  {
    return $this->belongsTo(District::class);
  }

  protected $fillable = [
    'district_id',
    'name'
  ];
}

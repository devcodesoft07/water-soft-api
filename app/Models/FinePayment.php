<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Fine;
use App\Models\Coin;

class FinePayment extends Model
{
  use HasFactory;
  protected $fillable = [
    'fine_id',
    'coin_id',
    'date'
  ];

  protected static function booted()
  {
    static::created(function ($finePayment) {
      $fine = Fine::find($finePayment->fine_id)->update(['paid' => true]);
    });
  }

  public function fine()
  {
    return $this->belongsTo(Fine::class);
  }

  public function coin()
  {
    return $this->belongsTo(Coin::class);
  }

  //scope functions
  public function scopeMonth($query, $month)
  {
    if ($month) {
      return $query->whereMonth('date', $month);
    }
  }

  public function scopeYear($query, $year)
  {
    if ($year) {
      return $query->whereYear('date', $year);
    }
  }
}

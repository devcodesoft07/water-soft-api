<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Expense;
use App\Models\FinePayment;
use App\Models\Revenues;

class Coin extends Model
{
  use HasFactory;

  protected $fillable = [
    'abbreviation',
    'name'
  ];

  public function expenses()
  {
    return $this->hasMany(Expense::class);
  }

  public function finePayments()
  {
    return $this->hasMany(FinePayment::class);
  }

  public function revenues()
  {
    return $this->hasMany(Revenues::class);
  }
}

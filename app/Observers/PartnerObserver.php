<?php

namespace App\Observers;

use App\Models\Community;
use App\Models\Partner;
use App\Services\CommunityService;
use App\Services\IdentityCardService;
use App\Services\PartnerService;
use App\Services\TelephoneService;
use Illuminate\Http\Request;

class PartnerObserver
{
  protected $request;
  protected $identityCardService;
  protected $telephoneService;
  protected $partnerService;

  public function __construct(Request $request, IdentityCardService $identityCardService, 
                    TelephoneService $telephoneService, PartnerService $partnerService)
  {
    $this->request = $request;
    $this->identityCardService = $identityCardService;
    $this->telephoneService = $telephoneService;
    $this->partnerService = $partnerService;
  }

  /**
   * Handle the Partner "created" event.
   *
   * @param  \App\Models\Partner  $partner
   * @param  IdentityCardService $identityCardService
   * @return void
   */
  public function created(Partner $partner)
  {
    $id = $partner->id;
    $this->identityCardService->register($this->request->only(['ci_number', 'complement', 'issued']), $id);
    $this->telephoneService->storeTelephones( $this->request->only( ['first_phone', 'second_phone', 'third_phone'] ), $partner);
  }

  /**
   * Handle the Partner "updated" event.
   *
   * @param  \App\Models\Partner  $partner
   * @return void
   */
  public function updated(Partner $partner)
  {
    if ($partner->eliminated == true) {
      $this->deleted($partner);
    }else {
      $identity_card = $this->request->only(['ci_number', 'complement', 'issued']);
      $phones = $this->request->only(['first_phone', 'second_phone', 'third_phone'] );
  
      $this->identityCardService->update($identity_card, $partner->identity_card->ci_number);
      $this->telephoneService->updateTelephone($phones, $partner);
    }
  }

  /**
   * Handle the Partner "deleted" event.
   *
   * @param  \App\Models\Partner  $partner
   * @return void
   */
  public function deleted(Partner $partner)
  {
    $meters = $partner->meters;      
    if ($meters) {
      $this->partnerService->desactivePartnerMeter($meters);
    }
  }

  /**
   * Handle the Partner "restored" event.
   *
   * @param  \App\Models\Partner  $partner
   * @return void
   */
  public function restored(Partner $partner)
  {
    //
  }

  /**
   * Handle the Partner "force deleted" event.
   *
   * @param  \App\Models\Partner  $partner
   * @return void
   */
  public function forceDeleted(Partner $partner)
  {
    //
  }
}

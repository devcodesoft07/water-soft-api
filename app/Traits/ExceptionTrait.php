<?php

namespace App\Traits;

use App\Traits\ApiResponser;

use Illuminate\Database\Eloquent\ModelNotFoundException;

trait ExceptionTrait {

  use ApiResponser;

  protected function exceptionCatched( $e, $message = null){

    $code = 500;

    if ($e instanceof ModelNotFoundException) {
      $code = 400;
    } else {
      $message = 'error en el servidor';
    }

    return $this->error(false, $message , $code );
  }
}
<?php

namespace App\Traits;

//models
use App\Models\Action;
use App\Models\ActionSale;
use App\Models\PaymentMethod;

use Throwable;

trait ActionSaleTrait
{

  public function recordActionSale(array $data, Action $action)
  {
    $pymnt_method = $data['payment_method'];

    $data['action_id'] = $action->id;
    $data['balance'] = 0;
    $data['debt'] = $action->price;
    
    try {

      $data['payment_method_id'] = PaymentMethod::where('name', $pymnt_method)->first()->id;
      
      $action_sale = ActionSale::create($data);

      return $action_sale;

    } catch (Throwable $th) {
      $action->delete();
      throw $th;
    }
  }
}
<?php

namespace App\Traits;

use App\Models\Coin;

use Throwable;

trait CoinTrait {

  public function getCoinId($coin)
  {
    try {
      $coin_id = Coin::where('abbreviation', $coin)->firstOrFail()->id;

      return $coin_id;
    } catch (Throwable $th) {
      throw $th;
    }
       
  }
}
<?php

namespace App\Traits;

//models
use App\Models\Action;

use Exception;

trait ActionTrait
{
  
  public function recordAction(array $data)
  {

    try {
      do {
        $data['action_code'] = $this->generateCode();
  
      } while (Action::where('action_code', $data['action_code'])->first());

      $action = Action::create($data);

    return $action;

    } catch (Exception $e) {
      throw $e;
    }
  }

  public function generateCode()
  {
    $character = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $number = '0123456789';

    $first = substr(str_shuffle($character), 0, 1);
    $second = substr(str_shuffle($number), 0,3);
    $third = substr(str_shuffle($character), 0,2);

    $code = $first.'-'.$second.'-'.$third;

    return $code;
  }
}
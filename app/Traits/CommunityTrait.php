<?php

namespace App\Traits;

use App\Models\Community;
use App\Models\District;

//exceptions
use Throwable;

trait CommunityTrait {

  public function storeCommunity($community_name) {
    
    try {

      $cmmnty = Community::where('name', $community_name)->first(); 

      if ($cmmnty) {
        return $cmmnty->id;
      }else {
        $new_community = Community::create([
          'district_id' => District::where('name', 'Distrito 4')->first()->id,
          'name' => $community_name
        ]);

        return $new_community->id;
      }

    } catch (Throwable $th) {
      throw $th;
    }
  }

  public function updateCommunity($data, $id)
  {
    $community_name = $data['community'];
    try {
      $community = Community::where('name', $community_name)->first();
 
      
    } catch (\Throwable $th) {
      //throw $th;
    }
  }
}
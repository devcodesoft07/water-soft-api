<?php

namespace App\Traits;

//models
use App\Models\ActionPayment;
use App\Models\Action;
use App\Models\ActionSale;
use App\Models\PaymentOption;
use Carbon\Carbon;

use Throwable;

trait ActionPaymentTrait {

  public function recordPaymentAction(array $data, int $action_sale_id)
  {
    try {
      $payment_option = $data['payment_way']; 
      $data['action_sale_id'] = $action_sale_id;
      $data['payment_option_id'] = PaymentOption::where('name', $payment_option )->first()->id;

      ActionPayment::create($data);

    } catch (Throwable $th) {
      $action_sale = ActionSale::find($action_sale_id);
      $action_id = Action::find($action_sale->action_id)->delete();
      $action_sale->delete();
      throw $th;
    }
  }

  public function isDebtor(int $partner_id)
  {
    $debtor = false;
    $action = Action::where('partner_id', $partner_id )->first();
    $payment = $action->actionSale->actionPayments->first();

    if ( $payment->next_payment > Carbon::now()->toDateTimeString()) {
      $debtor = ['debtor' => true, 'observation' => 'El socio no pago su siguiente couta de la action'];
    }

    return $debtor;
  }
}
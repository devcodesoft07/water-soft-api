<?php

namespace App\Traits;

use App\Models\Fine;
use App\Exceptions\FinePaidException;

trait FineTrait {

  public function paidFine($id): void
  {
    $fine = Fine::findOrFail($id);
    if ($fine->paid) {
      throw new FinePaidException('la multa con el id '.$id. ' ya fue cancelada', 1);
    }
  }
}
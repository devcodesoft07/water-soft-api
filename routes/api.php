<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\MeterController;
use App\Http\Controllers\MeterSaleController;
use App\Http\Controllers\PartnerController;
use App\Http\Controllers\ActionController;
use App\Http\Controllers\CommunityController;
use App\Http\Controllers\IdentityCardController;
use App\Http\Controllers\ExpenseController;
use App\Http\Controllers\ExpenseCategoryController;
use App\Http\Controllers\RevenueController;
use App\Http\Controllers\FineController;
use App\Http\Controllers\FinePaymentController;


Route::post('/register', [AuthController::class,'register']);
Route::post('/login', [AuthController::class,'login']);

Route::group(['middleware' => ['auth:sanctum']], function () {
  Route::post('/logout', [AuthController::class,'logout']);
});

Route::apiResources([
  'actions' => ActionController::class,
  'communities' => CommunityController::class,
  'expenses' => ExpenseController::class,
  'expense-categories' => ExpenseCategoryController::class,
  'fines' => FineController::class,
  'fine-payments' => FinePaymentController::class,
  'identity-cards' => IdentityCardController::class,
  'meters' => MeterController::class,
  'meter-sales' => MeterSaleController::class,
  'partners' => PartnerController::class,
  'revenues' => RevenueController::class,
]);

Route::get('/searhMeterNumber', [MeterController::class,'search']);
Route::get('/fines-partner', [FineController::class,'finesPartner']);
Route::get('/general-revenues', [RevenueController::class,'generalRevenues']);
